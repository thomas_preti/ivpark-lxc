intraparc=/opt/iv/intraparc/products/IntraParc
system=/opt/iv/intraparc/products/System

oldIP="192.1.2.250"
oldaddress="address $oldIP"
oldgw="gateway 192.1.2.254"
oldmask="netmask 255.255.255.0"
oldres="network 192.1.2.0"
oldnomhote="NomHote"

grep 'eth1\|eth0:0' /etc/network/interfaces
res=$?
if [ "$res" = "0" ]; then
	echo "Il y a plusieurs interfaces r�seau"
	echo "Le bridge doit �tre configur� manuellement"
	exit 1
fi

grep 'br0\|br1' /etc/network/interfaces
res=$?
if [ "$res" = "0" ]; then
        echo "Il y a d�j� un bridge"
        exit 1
fi

cat /etc/network/interfaces

active_ip=$(grep address /etc/network/interfaces | head -n 1 |  cut -c10-30)
active_gw=$(grep gateway /etc/network/interfaces | head -n 1 | cut -c10-30)
active_mask=$(grep netmask /etc/network/interfaces | head -n 1 | cut -c10-30)

echo "Param�tre par d�faut pour le bridge : "
echo "IP : $active_ip"
echo "Passerelle : $active_gw"
echo "Masque : $active_mask"
echo "Cette configuration est-elle correcte ? (o / n)"

read ans
if [ "$ans" != "o" ]; then
	echo "Saisir nouvelle IP"
        read ans
        newaddress="address $ans"
    
        echo "Saisir nouvelle passerelle"
        read ans
	gw=(`echo $ans | sed  s/"\."/" "/g`)
	newgw="gateway $ans"
																	                
        echo "Saisir le nouveau masque"
        read ans
        newmask="netmask $ans"
	masque=(`echo $ans | sed  s/"\."/" "/g`)
										                
	ip_res=$[${masque[0]} & ${gw[0]}]"."$[${masque[1]} & ${gw[1]}]"."$[${masque[2]} & ${gw[2]}]"."$[${masque[3]} & ${gw[3]}]
	newres="network $ip_res"
else
	masque=(`echo $active_mask | sed  s/"\."/" "/g`)
	gw=(`echo $active_gw | sed  s/"\."/" "/g`)
	ip_res=$[${masque[0]} & ${gw[0]}]"."$[${masque[1]} & ${gw[1]}]"."$[${masque[2]} & ${gw[2]}]"."$[${masque[3]} & ${gw[3]}]
	newres="network $ip_res"
		       
	newaddress="address $active_ip"
	newgw="gateway $active_gw"
	newmask="netmask $active_mask"
fi

echo "Bridge_Reseau.sh va configurer le r�seau en bridge"
echo "Les param�tres saisis sont les suivants : "
echo "  - Param�tres r�seau :"
echo "      - $newaddress"
echo "      - $newgw"
echo "      - $newmask"
echo "      - $newres"
echo "Valider (o / n)"
read ans
if [ "$ans" != "o" ]; then
	exit 1
fi

#Remplacement des anciennes valeurs
echo "Remplacement de l'adresse IP dans le fichier /etc/network/interfaces"

cp  /etc/network/interfaces  /etc/network/interfaces.bak

cp /opt/iv/scripts/conf/etc.network/original_bridge /etc/network/interfaces

cp /etc/network/interfaces /etc/network/interfaces.old
sed -e "s/$oldaddress/$newaddress/g" /etc/network/interfaces.old > /etc/network/interfaces

echo "Remplacement de la passerelle dans le fichier /etc/network/interfaces"
cp /etc/network/interfaces /etc/network/interfaces.old
sed -e "s/$oldgw/$newgw/" /etc/network/interfaces.old > /etc/network/interfaces

echo "Remplacement du r�seau dans le fichier /etc/network/interfaces"
cp /etc/network/interfaces /etc/network/interfaces.old
sed -e "s/$oldres/$newres/" /etc/network/interfaces.old > /etc/network/interfaces
	
if [ $# -eq 3 ]
then
	echo "Remplacement du masque dans le fichier /etc/network/interfaces"
	cp /etc/network/interfaces /etc/network/interfaces.old
	sed -e "s/$oldmask/$newmask/g" /etc/network/interfaces.old > /etc/network/interfaces
else
	echo "On garde le masque par d�faut $newmask"
fi
rm /etc/network/interfaces.old
echo "-> OK"
		echo ""
echo "Le bridge r�seau du serveur est configur�."

