HOTE=$1
IP=$2
MASQUE=$3
GATEWAY=$4
VETH=$5
MAC=$6
DEB_VERSION=$(lsb_release -cs)
#APT_SERV=$7

if [ $# -lt 6 ]
then
	if [ $# -gt 0 ]
	then
		if [ $1 == "-h" ]
		then
			 echo ""
			 echo "Script pour creation lxc"
		fi
	fi
	
	echo ""
	echo "Utilisation: install_LXC.sh NomHote IP masque passerelle interface_virtuelle MAC_ADRESSE"
	echo "exemple : install_LXC.sh vipfrxxxyyy 10.33.x.y 24 10.33.x.1 veth0 00:16:3e:a3:23:1d"
	echo ""

else
# Calcul des nouvelles valeurs
	echo "creation d'un conteneur LXC"
	echo "Les parametres saisis sont les suivants : "
	echo "  - Nom d'hote : $1"
	echo "  - Adresse IP : $2/$3"
	echo "  - Passerelle : $4"
	echo "  - insterface reseau virtuelle du LXC : $VETH"
	echo "  - MAC Adresse : $MAC"
	
	echo "Ces paramètres sont-ils corrects ? (oui/non)"
	read ans
	if [ "$ans" != "oui" ]; then
	        exit 1
	fi

    if [ "$DEB_VERSION" == "stretch" ]; then
            echo "installation d'un LXC Stretch"
			lxc-create -t download -n $1 -- -d debian -r stretch -a i386
            #SUITE=stretch MIRROR=http://ftp.fr.debian.org/debian lxc-create -n $1 -t debian
    else
            echo "installtion d'un LXC wheezy"
            lxc-create -n $1 -t debian -- -r wheezy
    fi

	sed -e "s/sys_time/sys_time sys_admin/g" /var/lib/lxc/$1/config
	echo "# reseau
lxc.network.type = veth
lxc.network.flags = up
lxc.network.link = br0
lxc.network.name = veth0
lxc.network.hwaddr = $MAC
lxc.network.mtu = 1500
lxc.network.ipv4.address = $2/$3
lxc.network.ipv4.gateway = $4" >> /var/lib/lxc/$1/config
	lxc-start -n $1 -d
	
	## Configuration du daemon
	rm /etc/init.d/$1-lxc
	sed -e "s/hostname-lxc/$1/g" /opt/iv/scripts/conf/etc.init.d/ivpark-lxc >> /etc/init.d/$1-lxc
	chmod +x /etc/init.d/$1-lxc
	update-rc.d $1-lxc defaults
	
	## Config DNS
	cp /etc/resolv.conf /var/lib/lxc/$1/rootfs/etc/.
	echo "$2	$1" >> /var/lib/lxc/$1/rootfs/etc/hosts
	
	## Copie scripts
	chroot /var/lib/lxc/$1/rootfs mkdir -p /opt/iv/scripts
	cp /opt/iv/scripts/install_vip.sh /var/lib/lxc/$1/rootfs/opt/iv/scripts/.
	cp -rp /etc/apt/sources.list* /var/lib/lxc/$1/rootfs/etc/apt/.
	chroot /var/lib/lxc/$1/rootfs chmod +x /opt/iv/scripts/install_vip.sh

	# Execution dans le LXC
	if [ "$DEB_VERSION" == "stretch" ]; then
        echo "installation du LXC Stretch fini"
    else
		chroot /var/lib/lxc/$1/rootfs apt-get update
		chroot /var/lib/lxc/$1/rootfs apt-get install iputils-ping vim wget -y
		chroot /var/lib/lxc/$1/rootfs dpkg-reconfigure locales
		echo "installation du LXC wheezy fini"
    fi
	
#	chroot /var/lib/lxc/$1/rootfs apt-get install ivpark-flybox-base -y
#	chroot /var/lib/lxc/$1/rootfs apt-get -f install
#	cp /opt/iv/scripts/installFS_Debian.sh /var/lib/lxc/$1/rootfs/opt/iv/scripts/.
#	chroot /var/lib/lxc/$1/rootfs chmod +x /opt/iv/scripts/installFS_Debian.sh
#	chroot /var/lib/lxc/$1/rootfs /opt/iv/scripts/installFS_Debian.sh
#	chroot /var/lib/lxc/$1/rootfs apt-get install vip-authority-base -y
#	chroot /var/lib/lxc/$1/rootfs apt-get download $(apt-cache depends --recurse --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances --no-pre-depends ivpark-flybox-base | grep "^\w")
#	chroot /var/lib/lxc/$1/rootfs cd /opt/iv/scripts/
#	chroot /var/lib/lxc/$1/rootfs ./installFS_Debian.sh
	exit
fi
