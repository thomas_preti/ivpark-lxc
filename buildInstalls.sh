#!/bin/bash
# Version  : $Rev: 3058 $ du $LastChangedDate: 2009-01-22 14:13:54 +0100 (jeu., 22 janv. 2009) $

echo "Current folder : `pwd`"

if [ $# != 4 ]; then
	echo "il faut 4 paramètres : PROJECTVERSION BUILDVERSION ARTIFACTID ROOTFOLDERSUFFIX"
	exit 1 
fi

PROJECTVERSION=$1
BUILDVERSION=$2
ARTIFACTID=$3
ROOTFOLDERSUFFIX=$4
echo "Versions : "	
echo "   $ARTIFACTID  : $PROJECTVERSION"
echo "   Build : $BUILDVERSION"

ROOT_DEP_FOLDER=target/$ARTIFACTID-$PROJECTVERSION-$ROOTFOLDERSUFFIX

echo "Checking environment : "

type fakeroot
if [ $? != 0 ]; then
	echo "package not found : fakeroot"
	echo "    aptitude install fakeroot"
	exit -2
fi
type dpkg
if [ $? != 0 ]; then
	echo "package not found : dpkg"
	echo "    aptitude install dpkg"
	exit -2
fi
echo "Ok, ready to build."

function getVersion() {
        #version=`find . -name "main*jar" | cut -d/ -f8 | cut -d- -f2,3,4 | cut -d. -f1,2,3,4`
        version="${PROJECTVERSION}-${BUILDVERSION}"
}
function build() {
        getVersion $1
        echo "Cleaning : $1_*.deb"
        rm -f "$1_*.deb"
        echo "OK, building $1 in version $version..."
		echo "PWD : `pwd`"
		echo "Hostname : `hostname`"
		echo " LS : `ls -al`"
		chmod -R 0775 DEBIAN
		
		head -n 3 DEBIAN/control
		sed -i "s/MAVEN_VERSION_TO_REPLACE/$version/g" DEBIAN/control
		head -n 3 DEBIAN/control
		
        echo "   cmd =  time fakeroot dpkg-deb -Zgzip --build  . $1_$version.deb> /dev/null"
        time fakeroot dpkg-deb -Zgzip --build  . $1_$version.deb
        echo "Build done : `pwd`"
}

echo "======================================================================"
echo "Building .deb ..."
echo "======================================================================"
	
cd $ROOT_DEP_FOLDER

build $ARTIFACTID-$ROOTFOLDERSUFFIX

cd -
mv $ROOT_DEP_FOLDER/$ARTIFACTID_*.deb target
err=$?
if [ $err != 0 ]; then
	echo "ERROR : with command ~/debianworkspace/1_build.sh ARTIFACTID"
	cd -
	exit $err
fi
cd -
echo "$ARTIFACTID .deb DONE"
